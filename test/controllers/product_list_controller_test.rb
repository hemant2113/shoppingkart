require 'test_helper'

class ProductListControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get product_list_index_url
    assert_response :success
  end

end
