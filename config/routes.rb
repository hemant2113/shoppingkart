Rails.application.routes.draw do
  devise_for :admins
  get 'product_list/index'

  resources :products
  get 'home/index'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
end
